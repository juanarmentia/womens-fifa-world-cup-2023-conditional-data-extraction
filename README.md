# Women's FIFA World Cup 2023 players' conditioning data collection

This Python script collects the players' conditioning data provided by FIFA in the Women's World Cup 2023. The result is
a .CSV with all the metrics, both normalized to 90 minutes and not normalized. 