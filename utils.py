import requests


conditional_desired_keys = ['AvgSpeed', 'DistanceHighSpeedRunning', 'DistanceHighSpeedSprinting', 'DistanceJogging',
                            'DistanceLowSpeedSprinting', 'DistanceWalking', 'AvgSpeed', 'SpeedRuns', 'Sprints',
                            'TopSpeed', 'TotalDistance', 'TimePlayed']
conditional_keys_to_90 = ['DistanceHighSpeedRunning', 'DistanceHighSpeedSprinting', 'DistanceJogging',
                          'DistanceLowSpeedSprinting', 'DistanceWalking', 'SpeedRuns', 'Sprints', 'TotalDistance']


def get_match_url(match_id):
    return f'https://fdh-api.fifa.com/v1/stats/match/{match_id}/players.json'


def extract_cond_data(match_json):
    cond_data_resp = requests.get(get_match_url(match_json['Properties']['IdIFES']))
    cond_data = cond_data_resp.json()
    clean_cond_data = {}
    [clean_cond_data.update({item: cond_data[item]}) for item in cond_data if len(cond_data[item]) > 10]
    return clean_cond_data


def process_cond_data(clean_cond_data, player_id):
    final_cond_data = {}
    for item in clean_cond_data[player_id]:
        if item[0] in conditional_desired_keys:
            final_cond_data[item[0]] = item[1]
    return final_cond_data


def normalize_to_90(processed_cond_data):
    cond_data_to_90 = {}
    for metric in conditional_keys_to_90:
        if metric in processed_cond_data.keys():
            cond_data_to_90[f'{metric}90'] = processed_cond_data[metric]/processed_cond_data['TimePlayed']*90
    return cond_data_to_90


def extract_players_data(match_json, location, players_list):
    url = f'https://api.fifa.com/api/v3/live/football/{match_json["IdMatch"]}?language=es'
    players_resp = requests.get(url)
    players_data = players_resp.json()
    team_name = players_data[location]['TeamName'][0]['Description']
    team_id = players_data[location]['IdTeam']
    match_desc = f'{match_json["HomeTeam"]} - {match_json["AwayTeam"]}'
    clean_cond_data = extract_cond_data(match_json)

    for player in players_data[location]['Players']:
        player_id = player['IdPlayer']
        if player_id in clean_cond_data.keys():
            final_cond_data = process_cond_data(clean_cond_data, player_id)
            cond_data_to_90 = normalize_to_90(final_cond_data)
            final_cond_data.update(cond_data_to_90)
            player_desc = {
                'IdPlayer': player_id,
                'PlayerName': player['PlayerName'][0]['Description'],
                'Position': player['Position'],
                'PlayerPicture': player['PlayerPicture']['PictureUrl'],
                'NameTeam': team_name,
                'IdTeam': team_id,
                'Match': match_desc
            }
            player_desc.update(final_cond_data)
            players_list.append(player_desc)