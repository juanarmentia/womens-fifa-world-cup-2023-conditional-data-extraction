import requests
import pandas

from utils import extract_players_data

match_desired_keys = ['Properties', 'Attendance', 'IdMatch']

matches_pre = []

matches_resp = requests.get('https://api.fifa.com/api/v3/calendar/matches?language=es&count=500&idSeason=285026')
macthes_data = matches_resp.json()
matches = macthes_data['Results']
matches = [match for match in matches if match['Attendance'] is not None]

for match in matches:
    result = dict((k, match[k]) for k in match_desired_keys if k in match)
    result['HomeTeam'] = match['Home']['TeamName'][0]['Description']
    result['AwayTeam'] = match['Away']['TeamName'][0]['Description']
    matches_pre.append(result)

players = []

for match in matches_pre:
    print(f'{match["HomeTeam"]} - {match["AwayTeam"]}')
    extract_players_data(match, 'HomeTeam', players)
    extract_players_data(match, 'AwayTeam', players)

players_df = pandas.DataFrame(players)
players_df.to_csv('players.csv', index=False)
